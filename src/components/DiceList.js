/*

components/DiceList.js - the list responble for showing the dice
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';
import {
  List,
  ListDivider,
} from '@rmwc/list';
import {v4 as uuidv4} from 'uuid';

import { genKey } from '../utils';

import DiceListItem from './DiceListItem';
  
const DiceList = (props) => {
    let listElems = [];
    let dice = [...props.dice];
 
    dice.sort((a,b) => {
        const keyA = genKey(a);
        const keyB = genKey(b);

        return keyA.localeCompare(keyB)
    });

    const diceLen = dice.length;

    for(let dieI = 0; dieI < diceLen; dieI++) {
      let die = dice[dieI];
      listElems.push(<DiceListItem {...die} key={genKey(die)}/>);
      if(dieI < (diceLen - 1)) {
        listElems.push(<ListDivider key={uuidv4()}/>);
      }
    }
    return (
      <List>
        { listElems }
      </List>
    );
}

const mapStateToProps = (state, props) => {
    const {dice} = state;
    return {
        dice,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {

    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiceList);
  