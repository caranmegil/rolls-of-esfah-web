/*

components/DiceListItem.js - the view responble for showing die details
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import {
    useState
} from 'react';

import {
  ListItem,
  ListItemText,
} from '@rmwc/list';

import {
    TextField
} from '@rmwc/textfield';

import {
    updateAmount
} from '../actions';

import '@rmwc/list/styles';
import '@rmwc/textfield/styles'
import { genKey } from '../utils';
import { useEffect } from 'react';

const DiceListItem = (props) => {
    let amount = 0;
    if (props.diceOwned[`${props.species}:${props.name}${(props.edition == '-') ? '' : ':' + props.edition}`] !== undefined) {
        amount = props.diceOwned[`${props.species}:${props.name}${(props.edition == '-') ? '' : ':' + props.edition}`];
    }

    const [amountOwned, setAmountOwned] = useState(amount);

    function onUpdateAmount(e) {
        setAmountOwned(parseInt(e.target.value));
        props.updateAmount(`${props.species}:${props.name}`, parseInt(e.target.value));
    }

    function formatEdition(species, edition) {
        let formattedEdition = edition;

        if (edition === '-') {
            formattedEdition = '';
        }

        return formattedEdition;
    }

    useEffect( () => {
        if (props.diceOwned[`${props.species}:${props.name}${(props.edition == '-') ? '' : ':' + props.edition}`] !== undefined) {
            setAmountOwned(props.diceOwned[`${props.species}:${props.name}${(props.edition == '-') ? '' : ':' + props.edition}`]);
        }
        
    }, [props.diceOwned[`${props.species}:${props.name}`]])

    return (
        <ListItem>
            <div className="dice-list-item">
                <ListItemText className={'species'}>{props.species}</ListItemText>
                <ListItemText className={'die-name'}>{props.name}</ListItemText>
                <ListItemText className={'edition'}>{formatEdition(props.species, props.edition)}</ListItemText>
                <ListItemText className={'type'}>{props.type}</ListItemText>
                <ListItemText className={'rarity'}>{props.rarity}</ListItemText>
                <TextField className={'amount'} onChange={onUpdateAmount} value={amountOwned} type="number" pattern="\d+"/>
            </div>
        </ListItem>
    )
}

const mapStateToProps = (state, props) => {
    const {diceOwned} = state;
    return {
        diceOwned,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
        updateAmount: (key, amount) => dispatch(updateAmount(key, amount)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiceListItem);
  