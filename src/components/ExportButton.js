/*

components/ExportButton.js - the button responble exporting the local store for import later
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { connect } from 'react-redux';

import {
    Button
} from '@rmwc/button';

import '@rmwc/button/styles';

const ExportButton = (props) => {
    function exportDice() {
        let filename = "MyDDCollection.json";
        let contentType = "application/json;charset=utf-8;";

        var a = document.createElement('a');
        a.style = "display: none;";
        a.download = filename;
        a.href = 'data:' + contentType + ',' + encodeURIComponent(localStorage.roll);
        a.target = '_blank';
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
  
    }

    return (
        <Button raised="true" label={'Export'} onClick={exportDice}/>
    )
}

const mapStateToProps = (state, props) => {
    const {dice} = state;
    return {
        dice,
    };
};

const mapDispatchToProps = (dispatch, props) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ExportButton);
  