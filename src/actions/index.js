/*

actions/index.js - the actions exported
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
    FILTER_DICE,
    IMPORT_ROLL,
    UPDATE_AMOUNT,
} from './types';

export const applyFilter = (filter) => {
    return {
        type: FILTER_DICE,
        filter,
    }
}

export const updateAmount = (key, amount) => {
    return {
        type: UPDATE_AMOUNT,
        key,
        amount,
    }
}

export const importRoll = (roll) => {
    return {
        type: IMPORT_ROLL,
        roll,
    }
}