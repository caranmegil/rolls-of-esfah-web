/*

App.js - the main component
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import {
    useState
} from 'react';

import {
    connect,
    dispatch
} from 'react-redux';

import {
    TopAppBar,
    TopAppBarRow,
    TopAppBarSection,
    TopAppBarTitle
} from '@rmwc/top-app-bar'

import {
    TextField
} from '@rmwc/textfield';

import {
    ListItemText,  
} from '@rmwc/list';


import DiceList from './components/DiceList';
import ExportButton from './components/ExportButton';

import {
    Button
} from '@rmwc/button';

import {
    applyFilter,
    importRoll,
} from './actions';

import '@rmwc/top-app-bar/styles';
import '@rmwc/tabs/styles';
import '@rmwc/typography/styles';
import '@rmwc/textfield/styles';
import '@rmwc/button/styles';
import './App.css';

const App = (props) => {
    let fileReader;
    function applyFilter(e) {
        props.applyFilter(e.target.value)
    }

    function handleImportFile(e) {
        props.importRoll(JSON.parse(fileReader.result));
    }

    function onFileChange(e) {
        fileReader = new FileReader();
        fileReader.onloadend = handleImportFile;
        fileReader.readAsText(e.target.files[0]);
    }

    function onClickImport(e) {
        document.getElementById('importRoll').click();
    }
    
    return (
        <div className="App">
            <TopAppBar>
                <TopAppBarRow>
                    <TopAppBarSection alignStart>
                        <TopAppBarTitle>Rolls of Esfah</TopAppBarTitle>
                    </TopAppBarSection>
                </TopAppBarRow>
            </TopAppBar>
            <section className={'App-section'}>
                Welcome to the Rolls of Esfah!  It is an inventory tracker for <a href="https://www.sfr-inc.com" target="_blank">Dragon Dice</a>.  Just filter for your die and increment 
                through the number at the end. To export it, press "EXPORT". To import from the same format, press "IMPORT".
                <div className="button-bar">
                    <div>
                        <input id="importRoll" type="file" onChange={onFileChange} /> 
                        <Button raised="true" onClick={onClickImport} label="Import"/>
                    </div>
                    <ExportButton />
                </div>

                <TextField label="Filter" onChange={applyFilter}/>

                <p/>

                <div className="dice-list-item dice-list-header">
                    <ListItemText className={'species'}>Species</ListItemText>
                    <ListItemText className={'die-name'}>Name</ListItemText>
                    <ListItemText className={'edition'}>Edition</ListItemText>
                    <ListItemText className={'type'}>Type</ListItemText>
                    <ListItemText className={'rarity'}>Rarity</ListItemText>
                    <ListItemText className={'amount'}>Amount Owned</ListItemText>
                </div>

                <p/>

                <div className="dice-list">
                    <DiceList/>
                </div>
            </section>
      </div>

    )
};

const mapStateToProps = (state, props) => {
    return {
    };
};
    
const mapDispatchToProps = (dispatch, props) => {
    return {
        applyFilter: (filter) => dispatch(applyFilter(filter)),
        importRoll: (roll) => dispatch(importRoll(roll))
    };
};
    
export default connect(mapStateToProps, mapDispatchToProps)(App);
      