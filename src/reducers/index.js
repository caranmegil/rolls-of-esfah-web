/*

reducers/index.js - the reducers
Copyright (C) 2021  William R. Moore <william@nerderium.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

import { combineReducers } from "redux";

import {
    UPDATE_AMOUNT,
    FILTER_DICE,
    IMPORT_ROLL,
} from '../actions/types';

import {
    data
} from './data';


const dice = (state = data, action) => {
    switch(action.type) {
        case FILTER_DICE:
            if (action.filter === '') {
                return data;
            } else {
                return data.filter( die => {
                    return die.species.toLocaleUpperCase().includes(action.filter.toLocaleUpperCase()) || die.name.toLocaleUpperCase().includes(action.filter.toLocaleUpperCase())
                });
            }
        default:
            return data
    }
};

const getLocalStorageDiceOwned = () => {
    return JSON.parse(localStorage.roll || '{}')
};

const diceOwned = (state = getLocalStorageDiceOwned(), action) => {
    switch(action.type) {
        case IMPORT_ROLL:
            localStorage.roll = JSON.stringify(action.roll);
            return action.roll;
        case UPDATE_AMOUNT:
            let roll = {...state}
            roll[action.key] = action.amount;
            localStorage.roll = JSON.stringify(roll);
            return roll;
        default:
            return state;
    }
};

export default combineReducers(
    {
        dice,
        diceOwned,
    }
);
  